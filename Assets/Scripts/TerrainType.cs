﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public struct TerrainType
{
    [Range(-1,1)]
    public float MinHeight, MaxHeight;
    public Color Color;
}
