﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    [Header("Generation Settings")]
    [SerializeField]
    private int _threads = 4;
    [SerializeField]
    private int _vertexSpacing = 10;
    [SerializeField]
    private Transform _parent;
    [Header("Map Settings")]
    [SerializeField]
    private int _chunkXSize = 20;
    [SerializeField]
    private int _chunkZSize = 20;
    [SerializeField]
    private int _viewDistance = 5;
    [SerializeField]
    private TerrainType[] terrainTypes;
    [Header("Noise Settings")]
    [SerializeField]
    private float _zoom = .3f;
    [SerializeField]
    private float _heightScale = 5f;
    [SerializeField]
    [Min(1)]
    private int _octaves = 3;
    [SerializeField]
    [Range(0.1f, 1f)]
    private float _gain = 0.5f;
    [SerializeField]
    private float _lacunarity = 2f;
    [SerializeField]
    private int _seed;

    public Material DefaultMaterial;

    public FastNoise FastNoise;
    private Queue<Thread> threadQueue = new Queue<Thread>();
    private Queue<TheadInfo<MapData>> finishedChunks = new Queue<TheadInfo<MapData>>();

    private void Start()
    {
        if (_seed == 0)
            _seed = UnityEngine.Random.Range(-10000, 10000);
        FastNoise = new FastNoise(_seed);
        FastNoise.SetNoiseType(FastNoise.NoiseType.PerlinFractal);
        FastNoise.SetFractalOctaves(_octaves);
        FastNoise.SetFractalGain(_gain);
        FastNoise.SetFractalLacunarity(_lacunarity);

        int half = _viewDistance / 2;
        for (int x = 0 - half; x < _viewDistance - half; x++)
        {
            for (int z = 0 - half; z < _viewDistance - half; z++)
            {
                GenerateChunk(x, z, SpawnChunk);
            }
        }

        for (int i = 0; i < _threads; i++)
        {
            StartCoroutine(ThreadQueue());
        }
    }

    private void GenerateChunk(int chunkX, int chunkZ, Action<MapData> callback)
    {
        Vector2 pos = ChunkToWorldPos(chunkX, chunkZ);
        threadQueue.Enqueue(new Thread(() => { GenerateMeshData(pos.x, pos.y, chunkX, chunkZ, callback, FastNoise, terrainTypes); }));
    }

    private void SpawnChunk(MapData data)
    {
        GameObject chunk = new GameObject($"Chunk {data.chunkX},{data.chunkZ}");
        chunk.transform.parent = _parent;
        chunk.transform.position = new Vector3(data.xPos * _vertexSpacing, 0, data.zPos * _vertexSpacing);

        Mesh mesh = new Mesh();
        Material material = new Material(Shader.Find("Standard"));
        Texture2D texture = new Texture2D(_chunkXSize + 1, _chunkZSize + 1);

        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.SetPixels(data.ColourMap);
        texture.Apply();
        material.mainTexture = texture;
        chunk.AddComponent<MeshFilter>().mesh = mesh;
        chunk.AddComponent<MeshRenderer>().material = material;

        mesh.vertices = data.vertices;
        mesh.triangles = data.trianges;
        mesh.uv = data.Uvs;
        mesh.RecalculateNormals();


        GameObject test = GameObject.CreatePrimitive(PrimitiveType.Plane);
        test.transform.localScale = new Vector3(100, 100, 100);
        test.GetComponent<MeshRenderer>().material = material;
    }

    public Vector2 ChunkToWorldPos(int x, int z)
    {
        return new Vector2(_chunkXSize * x, _chunkZSize * z);
    }

    private IEnumerator ThreadQueue()
    {
        if (threadQueue.Count > 0)
        {
            Thread lastThread = threadQueue.Dequeue();
            lastThread.Start();
            while (lastThread.IsAlive)
                yield return null;
            if (threadQueue.Count > 0)
                StartCoroutine(ThreadQueue());
        }
    }

    private void Update()
    {
        if (finishedChunks.Count > 0)
        {
            for (int i = 0; i < finishedChunks.Count; i++)
            {
                TheadInfo<MapData> info = finishedChunks.Dequeue();
                info.callback.Invoke(info.parameter);
            }
        }
    }

    private void GenerateMeshData(float xPos, float zPos, int chunkX, int chunkZ, Action<MapData> callback, FastNoise fastNoise, TerrainType[] terrainTypes)
    {
        Vector3[] vertices = new Vector3[(_chunkXSize + 1) * (_chunkZSize + 1)];
        Vector2[] uvs = new Vector2[vertices.Length];
        for (int i = 0, z = 0; z < _chunkZSize + 1; z++)
        {
            for (int x = 0; x < _chunkXSize + 1; x++, i++)
            {
                vertices[i] = new Vector3(x * _vertexSpacing,
                    fastNoise.GetNoise((x + xPos) * _zoom, (z + zPos) * _zoom) * _heightScale * _vertexSpacing,
                    z * _vertexSpacing);

                uvs[i] = new Vector2(x / (float)_chunkXSize, z / (float)_chunkZSize);
            }
        }

        Color[] colourMap = new Color[vertices.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            for (int c = 0; c < terrainTypes.Length; c++)
            {
                if (vertices[i].y > terrainTypes[c].MinHeight * _heightScale * _vertexSpacing &&
                    vertices[i].y < terrainTypes[c].MaxHeight * _heightScale * _vertexSpacing)
                {
                    colourMap[i] = terrainTypes[c].Color;
                    continue;
                }
            }
        }

        int[] triangles = new int[_chunkXSize * _chunkZSize * 6];
        int vert = 0;
        int tris = 0;
        for (int z = 0; z < _chunkZSize; z++)
        {
            for (int x = 0; x < _chunkXSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + _chunkXSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + _chunkXSize + 1;
                triangles[tris + 5] = vert + _chunkXSize + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }

        lock (finishedChunks)
        {
            finishedChunks.Enqueue(new TheadInfo<MapData>(callback,
                new MapData(vertices, triangles, xPos, zPos, chunkX, chunkZ, colourMap, uvs)));
        }

    }

    private struct TheadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;

        public TheadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }

    private struct MapData
    {
        public readonly Vector3[] vertices;
        public readonly int[] trianges;
        public readonly float xPos, zPos;
        public readonly int chunkX, chunkZ;
        public readonly Color[] ColourMap;
        public readonly Vector2[] Uvs;

        public MapData(Vector3[] vertices, int[] trianges, float xPos, float zPos, int chunkX, int chunkZ, Color[] colourMap, Vector2[] uvs)
        {
            this.vertices = vertices;
            this.trianges = trianges;
            this.xPos = xPos;
            this.zPos = zPos;
            this.chunkX = chunkX;
            this.chunkZ = chunkZ;
            ColourMap = colourMap;
            Uvs = uvs;
        }
    }
}

