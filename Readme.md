This project is where I'm experimenting with creating procedural terrain inside of unity using a [FastNoise](https://github.com/Auburns/FastNoise_CSharp) library.

Current Progress:
Generating a 70km size map
![Generating a large 70km map at runtime](eRJw40xt25.mp4)